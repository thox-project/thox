docs:
	@make -C docs html

preview-docs:
	@make -C docs livehtml

show-docs:
	@make -C docs show

.PHONY: docs preview-docs show
