thox, a simple multi-tasking OS for ComputerCraft
=================================================

thox is a simple operating system for `ComputerCraft: Tweaked`_ providing
pre-emptive multasking, secure thread sandboxing and simple IPC.

For more information, consult the following references:

* `Documentation <https://thox.madefor.cc/thox/>`_ (see the ``docs/``
  directory for sources).
* `Issues <https://gitlab.com/thox-project/thox/-/issues>`_, in case you're
  encountering problems or want to suggest what thox should add next.

Any question? Any issue? Any security flaw, e.g. a sandbox escape?
You can either contact me by mail at thomas@touhey.fr or on the
`Computer Mods Discord Server`_ (``@cake``).

.. _`ComputerCraft: Tweaked`: https://computercraft.cc/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
