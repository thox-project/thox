Code reference
==============

In this section, the direct code and code interfaces available in various
Lua environments are described in detail.

.. toctree::
    :maxdepth: 1

    code/initd
    code/runtimes
