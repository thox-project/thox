initd internals
===============

This document describes the inner workings of the thox process manager.
See :ref:`system-initd` for more information regarding the concepts.

.. lua:module:: initd

General constants
-----------------

.. lua:class:: QUANTUM: number

    The Round Robin quantum as a number of instructions to set to
    :lua:func:`debug.sethook`.

.. lua:class:: YIELD

    The yield types in the process manager coroutine hijacking system.

    .. lua:attribute:: BASIC: number

        The basic yield, which means the process has manually called its
        version of the :lua:func:`coroutine.yield` function. Can be returned in
        the process manager main loop if the process calls it from its main
        execution thread, and is then interpreted as
        :lua:attr:`YIELD.PREEMPT`.

    .. lua:attribute:: PREEMPT: number

        The pre-empt yield, automatically sent from the hook set by
        :lua:func:`debug.sethook` after a fixed number of instructions
        if the coroutine hasn't yielded yet.

    .. lua:attribute:: CALL: number

        The call yield, sent when the user calls :lua:func:`os.call`.

    .. lua:attribute:: ANSWER: number

        The answer yield, sent when the user calls :lua:func:`os.answer`.

    .. lua:attribute:: BIND: number

        The bind yield, sent when the user calls :lua:func:`os.bind`.

    .. lua:attribute:: UNBIND: number

        The unbind yield, sent when the user calls :lua:func:`os.unbind`.

Process Manager
---------------

.. lua:class:: Process

    A process as represented from the process manager's point of view.

    .. lua:attribute:: thread: thread

        The thread wrapping the function to execute, produced
        by :lua:func:`coroutine.create`.

    .. lua:attribute:: state: number

        The state the process currently is in, determining its treatment by
        the main process running loop, amongst one of the following:

        .. lua:attribute:: RUNNING: number

            The process is to be resumed on the next round.
            The code in the thread has its own environment for
            sandboxing purposes.

        .. lua:attribute:: WAITING: number

            The process is currently pulling events, which could be
            calls (if at least one RPC call is bound to it), answers,
            or events from queues the process is listening to.

        .. lua:attribute:: ZOMBIE: number

            The process has terminated or has been terminated, and is about
            to be removed.

    .. lua:attribute:: answer: table

        The arguments to return to the process on next resume, stored as a
        sequence. This is usually the answer to the last system call, or
        an empty table if the last yield was a pre-empt or equivalent.

        It is usually composed of:

        * A boolean indicating the status, ``true`` being that the system
          call has been successful and ``false`` signifying that an error
          has occurred.
        * The payload as a table. It is composed of the ``message`` member
          if it is an error, and free-format members depending on the system
          call if the call has been successful.

    .. lua:attribute:: events: table<os.Event>

        The list of events to be sent to the function, as a sequence from
        which to pop events each time.

    .. lua:attribute:: calls: table<cid, pm.Call>

        The array of calls managed by this process.

    .. lua:attribute:: outgoingCount: number

        The number of outgoing calls.

    .. lua:attribute:: incomingCount: number

        The number of incoming calls.

    .. lua:attribute:: alarms: table<pm.Alarm>

        A list of alarms which the process has summoned for itself.
        Alarms in this array must be sorted by alarm trigger time.

    .. lua:attribute:: commandLine: table<string>

        The command line with which the process has been invoked.
        First element should contain the name or path of the script.

    .. lua:method:: resume()

        Resumes the coroutine if it is sleeping.

.. lua:class:: Call

    A representation of a call from a process.

    .. lua:attribute:: type: number

        The type of the managed call, amongst the following:

        .. lua:attribute:: IN: number

            The call is an incoming call that awaits an answer from
            the current process.

        .. lua:attribute:: OUT: number

            The call is an outgoing call that awaits an answer from
            another process.

        .. lua:attribute:: ALARM: number

            The call is an outgoing call to the process manager to
            trigger an alarm after some time.

    .. lua:attribute:: pid: number

        The process identifier of the callee or the caller for
        incoming and outgoing calls.

    .. lua:attribute:: cid: number

        The call identifier on the other side (callee or caller, which pid
        is the one above) for incoming and outgoing calls.

.. lua:class:: Alarm

    An alarm as represented in the process manager.

    .. lua:attribute:: cid: number

        The call identifier which created the alarm, with which to
        respond when the alarm is through.

    .. lua:attribute:: alarmAt: number

        The clock value at or after which to trigger the alarm event as
        an answer to the process' call.

.. lua:class:: ProcessManager

    The process manager object, which manages processes, sandboxing,
    CPU time allocations, IPC, and permissions.

    .. lua:method:: spawn(program, options)

        Spawns a process and returns its Process IDentifier (PID).
        The process is spawned with the running state by default.

        .. todo:: Specify the options!

        :param program: The program as Lua code in textual form.
        :type program: str
        :param options: The options for executing a process.
        :type options: table

    .. lua:method:: kill(pid)

        Kills a process by settings its state to a zombie.

        .. todo:: Answer unanswered calls?

        :param pid: The PID of the process to kill.
        :type pid: number

    .. lua:method:: run()

        Run the processes until there are none left.
