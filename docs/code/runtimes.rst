Runtime APIs
============

thox provides a few libraries for helping to develop components for the
system. These sections describe libraries you have to ``require`` and are
not accessible through system calls.

.. toctree::
    :maxdepth: 2

    runtimes/native
