thox native process runtime API
===============================

When started, a thox process has access to a given set of functions and
constants; this document describes them.

.. todo::

    When started, every process should have access to the native functions
    to the current Lua function, e.g. those described in
    `the Lua 5.3 manual`_.

    Maybe make a table and check which utilities are available or not.

.. lua:module:: os

Process related calls
---------------------

.. lua:function:: run(func[, options])

    Runs the given function in a new process.

    The function must have only one upvalue: the ``_ENV`` variable, which
    will be replaced by the process's own environment.
    Any function with more upvalues will cause this call to fail.

    The options is a table containing the following fields:

    * ``ctx``: the identifier of the context to give to the new process as
      its default context. By default, this function takes the context 0
      from the current context.

    :param func: The function to run in another process.
    :type func: function
    :param options: The options table.
    :type options: table

.. lua:function:: run_file(path[, options])

    Equivalent of :lua:func:`os.run` with a preliminary step to read the
    file contents from a file, load it, and run it.

    This is actually not a system call by itself, but produces a function
    that loads the file using the default process utilities and runs it.
    An example function produced by this function is the following:

    .. code-block:: lua

        os.run(function ()
            local w = load(io.open("/path/to/script.lua"):read("a"),
                "/path/to/script.lua", "t")

            if w == nil then
                os.exit(false, true)
            end

            return w()
        end)

    By replacing the default context of the called process, and replacing the
    file access RPC calls, you will actually change the way the produced
    function will act.

    The options is a table containing the same fields as for
    :lua:func:`os.run`.

    :param path: The path to the Lua script to run.
    :type path: string
    :param options: The options table.
    :type options: table

.. lua:function:: run_code(code[, options])

    Equivalent of :lua:func:`os.run` with a preliminary step to load and
    run the given code.

    This is actually not a system call by itself, but produces a function
    that loads the file using the default process utilities and runs it.
    An example function produced by this function is the following:

    .. code-block:: lua

        os.run(function ()
            local w = load("print(\"hello world!\")", nil, "t")

            if w == nil then
                os.exit(false, true)
            end

            return w()
        end)

    By replacing the default context of the called process, and replacing the
    file access RPC calls, you will actually change the way the produced
    function will act.

    The options is a table containing the same fields as for
    :lua:func:`os.run`.

    :param code: The code to run in a separate process.
    :type code: string
    :param options: The options table.
    :type options: table

.. lua:function:: exit([code [, close]])

    Exit the current process.

    :param code: ``true`` (success) or ``false`` (failure).
    :type code: boolean
    :param close: *Unused*.
    :type close: boolean

Context related calls
---------------------

.. lua:function:: context()

    Creates a new context.

    Returns ``nil`` if the maximum number of contexts has been created for
    the current process.

    :return: The context identifier, or ``nil``.
    :rtype: number

.. lua:function:: close(ctx)

    Close a context bound to the current process. Calling this function will
    **fail** if:

    * The context doesn't exist.
    * The provided context corresponds to the default context (context 0).
    * The current process owns the given context, and the given context
      is currently opened by another process.

    :param ctx: A valid context identifier.
    :type ctx: number

Events
------

.. lua:class:: Event

    An event as seen by the user process.

    .. lua:attribute:: type: string

        The event type; see derivative classes.

Events can be one of the following:

.. lua:class:: CallEvent: Event

    An event emitted when receiving a call from another process
    through RPC.

    .. lua:attribute:: type: string

        The event type, set to ``"call"``.

    .. lua:attribute:: cid: number

        The Call IDentifier (CID) to which to answer.

    .. lua:attribute:: ctx: number

        The context identifier from which the call was emitted.

    .. lua:attribute:: name: string

        The name which was used to emit the call to the current process.

    .. lua:attribute:: pid: number

        The Process IDentifier (PID) of the process which has emitted
        the call.

    .. lua:attribute:: args: table

        The arguments to the function, as a sequence.

.. lua:class:: AnswerEvent: Event

    An event emitted when receiving an answer to a call emitted previously.

    .. lua:attribute:: type: string

        The event type, set to ``"answer"``.

    .. lua:attribute:: cid: number

        The Call IDentifier (CID) of the answered call.

    .. lua:attribute:: status_code: number

        The status code bundled with the answer; see :ref:`rpc-status-codes`
        for more information.

    .. lua:attribute:: args: table

        The arguments returned by the process.

.. lua:class:: OpenEvent: Event

    An event produced when a process opens a context owned by the current
    process (when not shared by the current process).

    .. lua:attribute:: type: string

        The event type, set to ``"open"``.

    .. lua:attribute:: ctx: number

        The identifier of the context which was shared.

    .. lua:attribute:: pid: number

        The Process IDentifier (PID) of the process which has gained
        access to the context.

.. lua:class:: CloseEvent: Event

    An event produced when a process closes a context owned by the
    current process (when not closed by the current process).

    .. lua:attribute:: type: string

        The event type, set to ``"close"``.

    .. lua:attribute:: ctx: number

        The identifier of the context which was shared.

    .. lua:attribute:: pid: number

        The Process IDentifier (PID) of the process which has gained
        access to the context.

These events are produced by the following function:

.. lua:function:: pull([filter[, ...]])

    Pull the next event out of the event queue for the process.

    The filters are tables containing expected values to filter the
    event with.

    For example, the following snippet waits for either:

    * An answer for the calls 4 and 5;
    * A new call.

    .. code-block:: lua

        local event = os.pull(
            {type = "answer", cid = 4},
            {type = "answer", cid = 5},
            {type = "call"},
        )

    When requiring a specific event, other events will stay in the queue
    for the next call asking specifically for it in the queue.

    :param filter: The filter as a table with the expected values.
    :type filter: table
    :return: The next event out of the event queue.
    :rtype: An :lua:class:`Event`.

Asynchronous RPC
----------------

.. lua:function:: call(ctx, name, ...)

    Emits a call to the given name on the given context.

    If the name ends with an underscore (see :ref:`rpc-names`), then the
    first returned value after the status code will either be ``nil`` or
    the number of a context shared with the current process.

    :param ctx: A valid context identifier.
    :type ctx: number
    :param name: A valid procedure name.
    :type name: str
    :return: The generated Call IDentifier (CID), or ``nil`` if an error
        has occurred.
    :rtype: number

.. lua:function:: answer(cid, status, ...)

    Answer a call with arguments.

    If the name of the related call ends with an underscore (see
    :ref:`rpc-names`), then the first returned value after the status code
    must be either ``nil`` or the number of the context to share.

    The status code should be 0 in case of success or an other value
    in case of failure; see :ref:`rpc-status-codes` for more information.

    :param cid: A valid Call IDentifier (CID).
    :type cid: number
    :param status: The status code for the call.
    :type status: number

.. lua:function:: transmit(ctx, name, cid[, pid])

    Transmit a call to another process bound to a given name on a given
    context.

    This function is an optimization: instead of having to make the call
    to the parent process and waiting for the answer to retransmit it
    to the end process, this actually sends the same call event (with the
    context and name replaced to correspond to this function's arguments)
    to the bound process, which will then answer directly to the client
    process; the CID is therefore closed from the current process's point
    of view when the call is transmitted.

    The ``pid`` parameter serves mostly when the call is not transmitted
    to the context owner specifically. This mostly serves for context-managing
    processes that support bindings, for them to transmit calls to
    children on a context they have created.

    .. todo::

        For now, we stay on the ``os.transmit`` logic, but soon we'll see
        that this approach may be slow when there are multiple levels of
        sandboxing. So what we could do is, have a nftables-like approach
        where a process owning a context configures routing for calls on
        the context (based on origin, name, why not arguments as well?),
        and the kernel doesn't need to call this process to route, whereas
        with the ``os.transmit`` we need to and this may be slow.

        However this is the complicated approach, for now we'll stick with
        the simpler approach as a PoC.

    :param ctx: A valid context identifier.
    :type ctx: number
    :param name: A valid RPC name.
    :type name: str
    :param cid: A valid Call IDentifier (CID).
    :type cid: number
    :param pid: The optional PID of the process to which the call should be
        transmitted on the given context.
    :type pid: number

Synchronous RPC
---------------

.. lua:class:: RPCProxy

    An RPC proxy object, which allows you to make synchronous RPC calls,
    on a given context.
    By calling this object, you emit a system call to the given name, and
    wait for an answer.

    Examples usages of this object are the following:

    .. code-block:: lua

        local status, div, rem = os.rpc.math.div(10, 3)

        -- is the equivalent of:

        local status, div, rem

        do
            local cid = os.call(0, "math.div", 10, 3)
            local status

            status, div, rem = os.pull(
                {type = "answer", cid = cid},
            )
        end

    .. lua:attribute:: any: RPCProxy

        Indexing the object gives you an RPC proxy object, on the same
        context and with a prefixed name, which you can call the same.

This class is instanciated as the following object:

.. lua:class:: rpc: RPCProxy

    The basic RPC proxy object, with a blank namespace.

Other functions
---------------

.. lua:function:: capture(co, ...)

    Equivalent of the :lua:meth:`coroutine.resume` function, but it captures
    system calls.

    :param co: The coroutine to resume.
    :type co: thread
    :return: The status of the coroutine, accompagnied with various data.

.. _remote procedure call: https://en.wikipedia.org/wiki/Remote_procedure_call
.. _typosquatting: https://en.wikipedia.org/wiki/Typosquatting
.. _the Lua 5.3 manual: https://www.lua.org/manual/5.3/manual.html
