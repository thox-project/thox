"""Configuration file for the Sphinx documentation builder.

For the full list of built-in configuration values, see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

from __future__ import annotations

import os

project = 'thox'
copyright = '2020-2021, Thomas Touhey'
author = 'Thomas Touhey'

release = 'latest'

primary_domain = 'lua'
extensions = [
    'sphinxcontrib.luadomain',
    'sphinx.ext.imgmath',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
]

templates_path = ['_templates']
exclude_patterns = ['_build', '**/Thumbs.db', '**/.DS_Store', '**/.*.kate-swp']

todo_include_todos = True

THROUGH_URL = os.environ.get('THROUGH_URL') or 'https://thox.madefor.cc/through/'
THUMB_URL = os.environ.get('THUMB_URL') or 'https://thox.madefor.cc/thumb/'

intersphinx_mapping = {
    'through': (THROUGH_URL, None),
    'thumb': (THUMB_URL, None),
}

html_theme = 'furo'
html_context = {
    'through_url': THROUGH_URL,
    'thumb_url': THUMB_URL,
}
html_title = 'thox, a modular multitasking system for ComputerCraft'
html_favicon = 'favicon.png'
html_logo = '_static/simple_logo.svg'
html_extra_path = ['favicon.ico']
html_math_renderer = 'mathjax'
