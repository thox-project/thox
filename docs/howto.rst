Howtos
======

In this section, you will be able to find tutorials for doing specific actions
using thox.

.. toctree::
    :maxdepth: 1

    howto/gps-cluster
    howto/transfer-file
