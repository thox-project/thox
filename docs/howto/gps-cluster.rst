How to set up a GPS cluster with thox?
======================================

.. todo::

    What I would like as an interface here is:

    * Define the position, and put the computers accordingly.
    * Add an ender modem.
    * Install thox.
    * Edit the GPS daemon configuration setting:

      - The automatic location to false.
      - The fallback to the computer's position.
      - The sharing to true.
