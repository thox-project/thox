How to transfer a file between two devices?
===========================================

.. todo::

    What I would like to do here is:

    * Set the receiving device to receive mode.
    * Go to a specific menu in the sending device.
    * Choose the file.
    * Confirm.
    * Accept on the receiving end.
