thox, a multi-tasking OS for ComputerCraft
==========================================

thox is an operating system for `ComputerCraft`_ providing
pre-emptive multitasking, secure thread sandboxing and modern IPC facilities.

For more information, consult the following references:

* `The repository <https://gitlab.com/thox-project/thox>`_.
* `The issues tracker <https://gitlab.com/thox-project/thox/-/issues>`_, in
  case you're encountering problems or want to suggest what thox should add
  next.

This documentation is organized using `Diátaxis`_' structure.

Any question? Any issue? Any security flaw, such as a sandbox escape?
You can either contact me by mail at thomas@touhey.fr or on the
`Computer Mods Discord Server`_ (``@cake``).

.. warning::

    This project is in early alpha and documentation-driven, which means the
    documentation will very probably be in advance on the code. This is because
    I strongly believe that **while the code might disappear, what will stay
    is the concepts and the user experience** attempted at in this project,
    and the notions covered in the documentation (including protocols' and
    file formats descriptions).

.. toctree::
    :maxdepth: 2

    onboarding
    howto
    topics
    code
    rpc

.. _Diátaxis: https://diataxis.fr/
.. _ComputerCraft: https://computercraft.cc/
.. _Computer Mods Discord Server: https://discord.com/invite/MgPzSGM
