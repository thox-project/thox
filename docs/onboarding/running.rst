Running thox
============

For now, the only method I use for running thox is using `CCEmuX`_.
In order to do this, copy or link the ``boot`` folder at the root
of your thox clone to be the root of a computer of your choice in
the emulator folders, e.g. ``~/.local/share/computer/<id>`` if you're
running a Linux-based distribution.

What I basically do is setting the link up on computer 1, then start
the emulator which starts with computer 0, and run the ``emu open 1``
command to run the system; it's faster to try again this way than
starting the emulator every time.

.. _CCEmuX: https://emux.cc/
