.. _fsd-rpc-api:

fsd (RPC API)
=============

The RPC calls provided by fsd are the following:

.. lua:module:: os.rpc.fs

.. lua:function:: open(path, mode)

    Open a file descriptor on the given path. The mode can be one of
    the following:

    ``"r"``
        Open the file for reading.

    ``"w"``
        Open the file for writing. Deletes the destination file if
        it already existed. Copy on write is used.

    In all cases, bytes will be returned; no byte transformation will be
    performed.

    The function returns the following:

    * If the request has succeeded, ``true`` followed by the numerical
      file descriptor to be used in subsequent calls.
    * If the request has failed, ``false`` followed by an error message.

    :param path: Path of the file to open.
    :type path: string
    :param mode: Opening mode.
    :type mode: string
    :return: The file context.
    :rtype: number

File descriptor calls
~~~~~~~~~~~~~~~~~~~~~

These calls happen on the file context provided by :lua:func:`os.rpc.fs.open`.

.. lua:module:: os.rpc.fs.file

.. lua:function:: read(count)

    Read ``count`` bytes from the file descriptor represented by its numerical
    identifier ``fd``. The function returns the bytes as a Lua string.

    :param count: Number of requested bytes.
    :type count: number
    :return: Up to ``count`` bytes.
    :rtype: string

.. lua:function:: write(bytes)

    Write the given bytes to the given file descriptor.

    :param bytes: The bytes to write.
    :type bytes: string
    :return: The number of written bytes.
    :rtype: number

.. lua:function:: sync()

    Synchronize the file's in-core state with the storage device
    (what others call “flushing” for write handles).

.. lua:function:: seek(whence, offset)

    Seek to a position.

    :param whence: "set", "cur" or "end".
    :type whence: string
    :param offset: The offset.
    :type offset: number
    :return: The new position.
    :rtype: number
