.. _gps-rpc-api:

gpsd (RPC API)
==============

.. lua:module:: os.rpc.gps

Errors are the following:

.. lua:class:: errors

    .. lua:attribute:: UNABLE: 1

        Unable to locate the device.

Functions are the following:

.. lua:function:: locate()

    Locate the device on Minecraft's three-dimensional cartesian coordinates
    system, using the GPS protocol or the position set manually.

    :return: The table containing the coordinates.
    :rtype: table
