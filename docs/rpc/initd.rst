.. _initd-rpc-api:

initd (RPC API)
===============

The process manager provides the initial process these RPC calls on its
default context:

.. lua:module:: os.rpc
    :noindex:

.. lua:function:: alarm(seconds)

    Ask for an alarm to be sent to you in a given number of seconds.
    This function is usually called for sleeping a certain time,
    by calling it then waiting for the answer.

    Example usage:

    .. code-block:: lua

        -- Sleep for five seconds and a half.
        rpc.alarm(5.5)
