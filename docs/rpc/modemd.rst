.. _modemd-rpc-api:

modemd (RPC API)
================

.. lua:module:: os.rpc.modem

.. lua:function:: list()

    List detected modems on all buses.

    :return: A list of strings representing the modems.
    :rtype: sequence

.. lua:function:: open_(modem_name)

    Open a context to the given modem.

    :param modem_name: The name of the modem to open.
    :type modem_name: string
    :return: A context to the modem.
    :rtype: context

.. lua:module:: os.rpc.modem.modem

.. lua:function:: get_port_(port)

    Get a port.

    .. todo::

        The interest of having a context by port is to emit messages on
        every ports, so that programs interested in messages for a specific
        port only get events considering that specific port.

    :param port: The port identifier, as a string or number depending on
        the modem type.
    :type port: any

.. lua:function:: list_open_ports()

    List opened ports.

    :return: The list of opened ports.
    :rtype: sequence

.. lua:module:: os.rpc.modem.modem_port

.. lua:function:: get_status()

    Get the status of the current port.

    :return: ``true`` if the port is opened, ``false`` otherwise.
    :rtype: bool

.. lua:function:: set_status(is_open)

    Set the status of the current port.

    :param is_open: Should the port be opened?
    :type is_open: bool

.. lua:function:: emit(payload, src_port)

    Emit a non-adressed message to the given port.

    :param payload: The payload.
    :type payload: any
    :param src_port: The source port to display.
    :type src_port: any

.. lua:function:: send(address, payload, src_port)

    Emit an adressed message to the given port.
    Note that all modem types do not support this method.

    :param address: Address of the host to which the message should be adressed.
    :type address: any
    :param payload: The payload.
    :type payload: any
    :param src_port: The source port to display.
    :type src_port: any

.. todo:: Develop and explain the modemd API.
