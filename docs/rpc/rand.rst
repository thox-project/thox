.. _rand-rpc-api:

rand (RPC API)
==============

.. lua:module:: os.rpc.random

The RPC calls provided by rand are the following:

.. lua:function:: random()

    :return: A random number within the ``[0.0, 1.0[`` range.
    :rtype: number

.. lua:function:: randint(min, max)

    :param min: Minimum value, included in the interval.
    :type min: number
    :param max: Maximum value, excluded from the interval.
    :type max: number
    :return: A random integer within the ``[min, max[`` range.
    :rtype: number
