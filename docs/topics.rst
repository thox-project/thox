Discussion topics
=================

The thox approach is that of a microkernel one, which is to separate as
much as possible distinct software components in order for the newcomer
to be able to understand them one by one without requiring two pages to
be opened at once.

In this section, you'll be able to find design documents and explanations
for each of the core components for thox.

.. toctree::
    :maxdepth: 2

    topics/initd
    topics/fsd
    topics/gpsd
    topics/modemd
    topics/rand

.. todo::

    Some services that could be useful in the future:

    * A redstone management daemon.
    * A turtle management daemon, for queuing actions and observation
      results and caching inventory content.
    * An HTTP and websocket management daemon.
    * A terminal/monitor management daemon, for managing inter-mod
      screen output and keyboard / clipboard input.
    * Virtualization guest package for common emulators, such as
      CCEmuX, for adding and removing peripherals, opening other
      machines, etc.
    * Sound management? Might require process priorities, depends on
      if the existing hardware has sound queues or not, but we need to
      run often even if that's the case.
    * Some database management daemon, supporting SQL?
    * `Cloud Catcher <https://github.com/SquidDev-CC/cloud-catcher>`_
      client daemon, or maybe that should be put in the terminal/screen
      management daemon with a dependency on HTTP/websocket?
    * Maybe Krist wallet management as well? Since it is popular
      amongst ComputerCraft players.
