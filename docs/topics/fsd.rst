.. _thox-fsd:

fsd: filesystem daemon
======================

The filesystem on thox is managed by fsd.

.. todo::

    This daemon occupies a special place as loading other binaries cannot
    take place without this one. How is this managed?

.. todo::

    Metadata file structure suggested by Lyqyd:

    http://www.computercraft.info/forums2/index.php?/topic/18646-rfc-metadata-file-structure-compatible-with-all-file-types/
