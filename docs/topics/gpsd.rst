.. _thox-gpsd:

gpsd: GPS locating daemon
=========================

On thox, determining the device's position and helping other devices to find
their position is permitted through the GPS daemon.

.. todo:: Link to the GPS protocol; see :ref:`through:modem-gps`.

.. todo::

    Have a configuration for it, telling it the following information:

    * Should we use the protocol to get our position?
    * What is our fallback, as a 3D Vector?
    * Should we share our position through the protocol.

    Describe what we mean by peer-to-peer, mostly in case of wireless modems
    only and no ender modems (since the latter are far too powerful…).

    Say its a bad idea to share the position when thox runs on a pocket
    computer.

    Manage position updates on turtle events.
