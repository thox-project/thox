.. _system-initd:

initd: process manager with thox IPC
====================================

.. todo::

    Introduce the concept of runtimes that run before the program main code
    to initialize the environment to a standardized environment using the
    thox native process API.

    Runtimes could include:

    * Native thox process runtime (nothing).
    * Extended compatibility (use the Lua current functions to give access
      to obsolete or future utilities, not just those of the current Lua
      function).
    * CraftOS runtime, which hides RPC calls between functions and constants
      described by the CC:T reference.
    * Other OS runtimes, such as OPUS runtimes.

    The goal is to ease program compatibility while retaining the security
    offered by the process design, as runtimes are constructs above the
    native process API. Kind of like "subsystems" in Windows NT.

    Such an option (e.g. ``runtime_path``) should be implemented in the
    :lua:func:`os.run`, :lua:func:`os.run_file` and :lua:func:`os.run_code`
    functions.

.. todo::

    Introduce a very simplistic method of storage for system management,
    with no need for a hierarchical filesystem. Is required for the current
    system design to work:

    * A single-level program store, where each program is represented
      as a name.
    * A single-level library store, where each library is represented
      as a name and can be used with ``require()`` (although must be
      single-file).
    * A single-level runtime store, where each runtime is represented
      as a name; although this might not be required? But would be
      appreciated so that no "program wrapper" has to be done and
      runtimes could be used directly on ``os.run`` and command-line
      (e.g. ``cc#program_name``).
    * A per-program single rewritable stream for storing
      application-specific data, e.g.:

      - Service configuration and state in ``initd``.
      - Modem configuration in :ref:`thox-modemd`.
      - Packages database and system state in ``pkgd``.
      - etc.

      Could also be interesting to have locks; maybe a Redis-like (although
      simpler, obviously) memory?

      Although let's say ``os.run_program`` uses
      ``os.run_code(load(os.get_program(...)))``. How does thox
      know which persistent storage space to give the process since it
      doesn't have the program's name? Should we let highly privileged
      processes decide of that, and not make it something thox manages,
      so have say a rewritable block device provided by the startup script
      and ``initd`` (or a process underneath it, e.g. ``storaged``)
      could provide RPC calls to get a context on which stream-like or
      key-value database operations could occur?

    This would change the thox program API as ``os.run_file`` would have
    to be replaced with ``os.run_program`` and/or ``os.get_program``,
    with the addition of ``os.set_program``, ``os.get_library``,
    ``os.set_library``, ``os.get_runtime``, ``os.set_runtime``.

    Although who can run ``os.set_program``, ``os.set_library`` and
    ``os.set_runtime``? Shouldn't we just make these RPC calls managed
    by thox itself? Chicken and egg problem, that is.

thox provides multitasking_, which means that processor time is shared
between multiple tasks, grouped in processes, automatically.

.. note::

    This differs massively from CraftOS, which implements a DOS-like
    monotasking system which implements `Terminate and stay
    resident programs`_.

When possible, thox multitasking will be `pre-emptive`_, which means that
switching between processes can be done without requiring the process to
give control back to the system; however, in some circumstances, this cannot
be accomplished, therefore thox resorts to simple cooperative_ multitasking,
which requires process to give control back to the system through any
system call. For more details, see :ref:`system-multitasking-using-lua`.

In the following sections, we will discuss the following points:

* The concepts used in this project: processes, contexts and interactions.
* The native thox runtime API.
* RPC APIs of native contexts, managed by the thox process manager.

Then, if you want to go more in depth about the thox process manager, you can
find information in the following sections:

* How running and switching between processes is implemented.
* The manager implementation reference, i.e. how the manager is implemented
  in terms of functions and objects.

See all of these discussions and references in the following sections:

.. toctree::
    :maxdepth: 2

    initd/contexts
    initd/manager

.. _Terminate and stay resident programs:
    https://en.wikipedia.org/wiki/Terminate_and_stay_resident_program
.. _multitasking: https://wiki.osdev.org/Multitasking_Systems
.. _pre-emptive:
    https://wiki.osdev.org/Multitasking_Systems#Preemptive_Multitasking
.. _cooperative:
    https://wiki.osdev.org/Multitasking_Systems#Cooperative_Multitasking
