thox process manager
====================

The thox process manager, provided through the ``thox`` package, provides
the main process manager which implements the concept of processes and
switches between them.

.. _system-multitasking-using-lua:

How this is accomplished in Lua
-------------------------------

Lua facilities we use in order to implement the thox process manager are
the following:

* `Coroutines`_;
* `debug.sethook()`_.

Coroutines are basically functions that can start and stop as needed. You
give them thread control by “resuming” them, and they give you control back
when they have a value for you, which is called “yielding”, when an error
occurred and they didn't catch it, or when the function terminated its
execution. They do not run concurrently; they resemble what Python calls
`generators`_, although Python has `coroutines of its own
<Python coroutines_>`_.

Coroutines, however, only sets ground for a `cooperative multitasking`_
system. In order to make it pre-emptive, we need to force the processes to
yield after a certain time, e.g.:

.. code-block:: lua

    local function hook_autoyield(type, arg)
        coroutine.yield(YIELD.PREEMPT)
    end

    debug.sethook(co, hook_autoyield, "", QUANTUM)

Debug hooks are actually defined on a per-coroutine basis, defining them
for all subcoroutines can only be made by calling :lua:func:`debug.sethook`
when the subcoroutine is created. A proof-of-concept of this is the following:

.. literalinclude:: setcohook-test.lua
    :language: lua
    :linenos:

Which gives the following result:

.. image:: setcohook-demo.png
    :align: center

As you can see, the hook is only called for the coroutine it was set into.

.. todo::

    Finish describing this, and express in this section how debug utilities
    aren't always present in the environments we support...

System calls and coroutines
---------------------------

In order to set up all of this, the process manager and the various processes
must be able to communicate. This is done thanks to coroutines and yielding.
If you haven't done it, please read the chapter about `Coroutines`_.

As explained previously, a process is a coroutine wrapped with some information
about when and how to resume it; see :lua:attr:`startup.Process.thread`.
The BIOS actually does the same thing; see the
`os.pullEventRaw() definition`_.

However, we want to do it better than the BIOS, by allowing the user to:

* Make a system call from a nested coroutine within the process,
  without the need for every intermediate level in the coroutine stack to
  transmit it explicitly.
* Have a coroutine waiting for an answer while others (or the upper level)
  can continue running.

In order to achieve this, thox hijacks the normal yielding system by replacing
the :lua:func:`coroutine.yield` and :lua:func:`coroutine.resume` to add a
special value at the beginning, invisible to the user, which represents the
yield type amongst:

* **basic yield**: this means the process has manually called its version
  of the :lua:func:`coroutine.yield` function.
  This can also happen when the function calls :lua:func:`coroutine.yield`
  from its main thread; this is then understood the same as a pre-empt yield.
* **preempt yield**: this means the process has taken too long before
  yielding, and the auto-yielding function has fired because the quantum
  has been reached.
* **system call yield**: this means the process has made a system call,
  amongst :lua:func:`os.call`, :lua:func:`os.answer`, :lua:func:`os.bind`
  and :lua:func:`os.unbind`.

Note that the user doesn't have access to the “real” coroutine functions,
hidden behind the hijacked coroutine functions and the system calls described
above. Note also that there is one yield that doesn't include the special
value, and that is the case when the coroutine dies with arguments; any
code within these functions or the process manager must first check if the
coroutine has died or not to know if the special value is present or not.

Based on these elements, you should now know what the hijacked coroutine
functions bring on top of the system ones:

* :lua:func:`coroutine.yield` adds the :lua:attr:`pm.YIELD.BASIC` special
  value before the arguments given by the user, and calls the real yield
  function.
* :lua:func:`coroutine.resume` resumes the coroutine using the real
  resuming function until it yields. Then:

  * If the coroutine has died, then it returns the received arguments
    received from the real “resume” function to the caller, as the special
    value isn't here.
  * Otherwise, if the coroutine has yielded, then it extracts the special
    value. If this value indicates a normal yield, then the arguments
    initially passed to :lua:func:`coroutine.yield` to the caller.
  * Otherwise, it yields the arguments accompagnied with the special
    value in the same fashion that it has received them using the
    real “yield” function.

All of this behaviour is transparent to the user. Note that instead of calling
:lua:func:`coroutine.resume`, any code in the process can instead call
:lua:func:`os.capture`, which does the same thing except it does not
automatically yield system calls but returns them to the caller, which
can then imitate the system (useful for testing), transmit the calls and
answers while sharing time between coroutines in a collaborative fashion
(useful when making daemons), log system calls and answers in `strace`_
fashion, and so on.

.. todo::

    Explain sandboxing, referencing context switching (use the link below).

    Also, I should shift from loading the program outside with a distinct
    environment to start a process directly, and setup the sandbox and
    load the file from within the created function! This would allow me
    more freedom and make the whole thing simpler; this means that
    the :lua:meth:`startup.ProcessManager:add` method would only
    take a function (instead of a program) and run it inside a coroutine.

Startup and main loop
---------------------

At computer startup, the BIOS is loaded (usually CraftOS, present in the ROM;
see `bios.lua`_). Then, if the related ComputerCraft settings are set
appropriately, the ``startup.lua`` file from thox is executed; the related
setting is either ``shell.allow_startup`` if thox is installed on the
computer's main disk, or ``shell.allow_disk_startup`` if thox is installed
on a disk in a peripheral disk drive.

Once started, the startup script from thox creates the process manager,
named ``pm``, spawns the initial processes (i.e. basic drivers and the init
script) using :lua:meth:`startup.ProcessManager.spawn`, then enters the main
loop through :lua:meth:`startup.ProcessManager:run`.

The main loop consists of the following steps:

#. Check on each process.
#. Remove the zombies. If there are no processes left, exit the
   main loop and shutdown (this is not considered normal).
#. Read hardware events during:

   * During the hardware quantum if some processes are running;
     i.e. the game tick duration, .05 seconds.
   * Otherwise, until the next alarm trigger or the next
     hardware events that occurs and is pulled by at least
     one process.

Step 1 runs as follows:

#. If the process has any alarm set, check if any has already
   gone past. If that's the case, add an event for each alarm
   that has gone past (as an answer to the call using its CID).
#. If the process is currently waiting and there is an event
   waiting for the process to receive, set the answer to the
   received event (as an :lua:class:`os.Event`) and set the
   process to be running.
#. Then, if the process is currently running:

   a. Resume it using the latest answer to give it to.
   b. Check the status of the coroutine:

      * If the process has died (the task is finished, or an uncaught
        error has occurred), set the process to be a zombie.
      * If the process has yielded normally (the user has called a raw
        ``coroutine.yield()``) or has been pre-empted, we ignore the
        call and continue on with our loop.
      * If the process has emitted a call, we attribute a system-wide
        unique identifier to that call, and transmit the request to the
        appropriate process; if there was none, set the next answer to
        ``false``.
      * TODO: bind and answer?

.. todo::

    How does this procedure manage the events? Event filtering? Function
    binding? Too many calls? Priorities (if any)? Passing calls? Alarms?
    Many things are to define in this process.

    What about hanging calls when a process is killed? Is there a possibility
    to cancel a call once started, either by process will or when the process
    is killed?

.. _pre-emptive multitasking: https://wiki.osdev.org/Multitasking_Systems#Preemptive_Multitasking
.. _cooperative multitasking: https://wiki.osdev.org/Multitasking_Systems#Cooperative_Multitasking
.. _Python coroutines: https://docs.python.org/3/library/asyncio-task.html
.. _Coroutines: http://www.lua.org/manual/5.3/manual.html#2.6
.. _debug.sethook(): http://www.lua.org/manual/5.3/manual.html#pdf-debug.sethook
.. _context switching: https://wiki.osdev.org/Context_Switching
.. _generators: https://wiki.python.org/moin/Generators
.. _Round Robin: https://wiki.osdev.org/Scheduling_Algorithms#Round_Robin
.. _bios.lua: ttps://github.com/cc-tweaked/CC-Tweaked/blob/b97e950d86e4694409c01e507db34a87da85e452/src/main/resources/data/computercraft/lua/bios.lua
.. _os.pullEventRaw() definition: ttps://github.com/cc-tweaked/CC-Tweaked/blob/b97e950d86e4694409c01e507db34a87da85e452/src/main/resources/data/computercraft/lua/bios.lua#L104
.. _strace: https://linux.die.net/man/1/strace
