local t = coroutine.create(function ()
    debug.sethook(function (type, arg)
        print("{HOK} debug hook was called!")
    end, "", 5e4)

    for i = 1, 2e5 do
        if i % 5e4 == 0 then
            print("[lv2] currently at:", i)
        end
    end

    local st = coroutine.create(function ()
        for i = 1, 2e5 do
            if i % 5e4 == 0 then
            print("[lv3] currently at:", i)
            end
        end
    end)

    coroutine.resume(st)
end)

coroutine.resume(t)

for i = 1, 2e5 do
    if i % 5e4 == 0 then
        print("[lv1] currently at:", i)
    end
end
