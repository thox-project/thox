-- torpcname.lua, a module defining an RPC name validation function.
--
-- Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thox project, which is MIT-licensed.
--
-- For more information, consult the following page:
-- <https://thox.madefor.cc/system/process/contexts.html#rpc-names>_

function toRPCName(name)
local name = string.lower(tostring(name))
local endsWithUnderscore = false
local forbiddenComponents = {'and', 'break', 'do',
'else', 'elseif', 'end', 'false', 'for', 'function', 'goto',
'if', 'in', 'local', 'nil', 'not', 'or', 'repeat', 'return',
'then', 'true', 'until', 'while'}

-- We check here if the name finishes with an underscore.

if string.sub(name, -1) == "_" then
endsWithUnderscore = true
name = string.sub(name, 1, -2)
end

-- Checks being made here:
--
--  * No character other than letters, digits and dots are present.
--  * No name component start with a digit.
--  * No name component is empty.
--  * No name component is a reserved word.

if string.match(name, '^[a-z0-9.]*$') ~= name then
error("there is a forbidden character")

return nil
end

do
local cname = "." .. name .. "."

if string.match(cname, '[.][0-9]') then
error("a name component starts with a digit")

return nil
elseif string.find(cname, '[.][.]') ~= nil then
error("a name component is empty")

return nil
end

for _, component in pairs(forbiddenComponents) do
if string.find(cname, '[.]' .. component .. '[.]') ~= nil then
error("a forbidden name component was found: " .. component)

return nil
end
end
end

if endsWithUnderscore then
name = name .. '_'
end

return name
end

-- Tests for checking if the function defined above works.

for _, name in pairs({'sleep', 'os.module', 'how.deep.DOES.this.go',
'my.function2', 'my.function2_', 'my_', '_', 'for', '123hello',
'hello.2theworld', 'my.gawd$', 'my_.function2', 'my._', 'my..god',
'FS.GetSpaceLeft', 'FS.GETSPACELEFT', 'fs.getspaceleft'}) do
local status, result

status, result = pcall(toRPCName, name)
if status then
print('  ' .. name .. ' - ' .. result)
else
print('! ' .. name)
print('> ' .. result)
end
end
