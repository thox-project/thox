.. _thox-modemd:

modemd: modem networking daemon
===============================

The network on thox is managed by modemd.

.. todo:: Interfaces, etc.

.. todo::

    What I expect from this program:

    * Interfaces, usually linked to a physical device,
      with a predictable address such as ``ccsl`` (for ComputerCraft
      modem on left side) or ``ccslw2`` (for ComputerCraft modem on
      second port of left side wired bus).

      With support for:

      - ComputerCraft modems (cc).
      - OpenComputers modems (oc).
      - Skynet modems (sn).
      - Rednoot modems (rn).
    * Opening and closing ports.
    * Sending and receiving messages on said ports.
    * Message monitoring (independent from the subsystems).
    * Subsystems which catch received messages and prevent sending messages
      on higher-level subsytems.

      For a good exercise of the daemon's design, try implementing the
      following subsystems:

      - Rednet.
      - OneOS protocols.
      - OPUS OS protocols.
      - LyqydNET.
      - Other port protocols.
