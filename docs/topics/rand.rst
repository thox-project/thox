.. _thox-rand:

rand: random and entropy production daemon
==========================================

.. todo::

    Describe how this daemon manages its entropy and everything;
    and see :ref:`through:randomness` for more information.

.. todo::

    Write about randomness and entropy generation in ComputerCraft,
    use sources currently listed in OPUS and OneOS protocols' description.
    Mersenne Twister as well?

    Also add information about pure Lua cryptography, used algorithms
    amongst protocols, how effective they are, how proved they are or not,
    which LuaRocks modules they depend on, etc.
