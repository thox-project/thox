-- daemon.lua, a library for defining rpc daemons for thox.
--
-- Copyright (C) 2020-2021 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thox project, which is MIT-licensed.
--
-- This library is made for easing writing daemons without making the
-- system calls manually using `os`.

local exports = {}

do
    -- First of all, define the namespace management objects for ease of use.
    local function toValidRPN(parentName, name)
        -- This function defines a namespace proxy for binding
        -- a proxy.
        --
        -- First, we ought to check if the current namespace
        -- ends with an underscore; if that is so, we cannot
        -- get the sub namespace, as it should be the end.
        if parentName:sub(-1) == "_" then
            return nil
        end

        -- Then, we ought to check if the key is valid, by
        -- going through the following steps:
        --
        --  * Check if the name uses valid characters all along.
        --  * Check if it isn't one of the forbidden words.
        key = string.lower(tostring(key))
        if string.match(key, "[a-z][a-z0-9]*_?") ~= key then
            return nil
        end

        do
            local reservedNames = {"", "and", "break", "do",
                "else", "elseif", "end", "false", "for",
                "function", "goto", "if", "in", "local", "nil",
                "not", "or", "repeat", "return", "then", "true",
                "until", "while"}

            for _, reserved in ipairs(reservedNames) do
                if name == key then
                    return nil
                end
            end
        end

        -- We have a valid subnamespace, let's make a proxy.
        if parentName ~= "" then
            key = parentName .. "." .. key
        end

        return key
    end

    local function namespaceFunctions(functionTable, ns)
        local functions = {}

        -- Two different loops instead of one with a big conditional
        -- in order to optimize the copy in most use cases.
        if ns == "" then
            for name, func in pairs(functionTable) do
                functions[name] = func
            end
        else
            for name, func in pairs(functionTable) do
                if name == ns or string.sub(ns, 1, #ns + 1) == ns .. "." then
                    functions[name] = func
                end
            end
        end

        return functions
    end

    local function onCallNamespace(functionTable, ns)
        return setmetatable({
            _astable = function ()
                return namespaceFunctions(functionTable, ns)
            end
        }, {
            __index = function (self, key)
                -- TODO: here, we could have multiple components at once
                -- in the key, so we shouldn't just check the component...
                key = toValidRPN(ns, key)
                if key == nil then
                    return nil
                end

                return rpcNamespace(functionTable, key)
            end,

            __newindex = function (self, key, value)
                if type(value) ~= "function" then
                    return
                end

                key = toValidRPN(ns, key)
                if key == nil then
                    return
                end

                functionTable[key] = value
            end,

            __call = function (self, ...)
                local func = functionTable[key]

                if func == nil then
                    func = function (...)
                        return 253, "This function is not bound."
                    end
                end

                return func(...)
            end,

            __len = function (self)
                return #namespaceFunctions(functionTable, ns)
            end,

            __tostring = function ()
                return ns
            end
        })
    end

    -- Daemon object definition.

    exports.daemon = function ()
        local contexts = {}

        local function context(id)
            if contexts[id] ~= nil then
                return contexts[id]
            elseif id == nil then
                return nil
            end

            local data = {
                id = id,
                refs = 0,
                oncall = onCallNamespace({}, ""),
                onclose = nil,

                -- Static functions accessible through either context,
                -- including the default one.

                context = function ()
                    -- Return a context with a given number.

                    return context(os.context())
                end,

                run_forever = function ()
                    -- Bind the functions to the current process from the
                    -- kernel view.

                    local boundOne = false

                    for key, _ in pairs(contexts[0]._astable()) do
                        os.bind(key)
                        boundOne = true
                    end

                    if not boundOne then
                        return false
                    end

                    -- From here, we have the simplest multi-call manager.
                    -- We use cooperative at the daemon-level, as there is
                    -- already a pre-empt at system level, so it would be
                    -- overkill.

                    local awaitingAnswers = {}

                    while true do
                        local event = os.pull()
                        local co = nil, args = nil, cid = nil

                        -- TODO: doesn't behave properly; should intercept
                        -- os.pull() from subroutines to be perfectly
                        -- compatible with rpc libraries...

                        if event.type == "call" then
                            local ctx = contexts[event.ctx]

                            if ctx == nil then
                                os.answer(event.cid, 253,
                                    "This context has no bound functions.")
                            else
                                -- TODO: we would also want to give request
                                -- details...

                                co = coroutine.create(function (...)
                                    ctx.oncall[event.name]()
                                end)
                                args = table.unpack(event.args)
                                cid = event.cid
                            end
                        elseif event.type == "answer" then
                            co, cid = table.remove(awaitingAnswers, event.id)
                            args = event.args
                        elseif event.type == "open" then
                            local ctx = contexts[event.ctx]

                            if ctx ~= nil then
                                ctx.refs = ctx.refs + 1
                            end
                        elseif event.type == "close" then
                            local ctx = contexts[event.ctx]

                            if ctx ~= nil then
                                local refs = ctx.refs - 1
                                if refs > 0 then
                                    ctx.refs = refs
                                else
                                    contexts[event.ctx] = nil

                                    if ctx.onclose ~= nil then
                                        co = coroutine.create(function ()
                                            ctx.onclose()
                                        end)
                                        args = {}
                                    end
                                end
                            end
                        end

                        if co ~= nil then
                            -- Here, we want to run the coroutine and intercept
                            -- system calls.

                            -- TODO: if we return a context, we should
                            -- increment the context reference counter.

                            local ret = table.pack(os.capture(co,
                                table.unpack(args)))
                            local is_call = table.remove(ret, 1)

                            if is_call then
                                cid = os.call(table.unpack(ret))
                                awaitingAnswers[cid] = {co, cid}
                            else

                            end
                        end
                    end
                end
            }

            local cls = setmetatable(data, {
                __newindex = function (self, key, value)
                    if key == "onclose" then
                        -- TODO: check if it is a function
                        data[key] = value
                    elseif key == "refs" then
                        -- TODO: check if it is an integer
                        data[key] = value
                    end
                end
            })

            contexts[id] = cls
            return cls
        end

        return context(0)
    end
end
