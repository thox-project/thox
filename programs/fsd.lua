-- a filesystem RPC daemon for thox.
--
-- Copyright (C) 2021 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thox project, which is MIT-licensed.

local daemon = require "daemon".daemon()

daemon.oncall.fs.open_ = function (path, mode)
    local ctx = daemon.context()
    local handle = io.open(path, mode)

    ctx.onclose = function ()
        handle:close()
    end

    ctx.oncall.read = function (count)
        return 0, handle:read(count)
    end

    ctx.oncall.write = function (bytes)
        return 0, handle:write(bytes)
    end

    ctx.oncall.sync = function ()
        return 0
    end

    ctx.oncall.seek = function (whence, offset)
        handle:seek(whence, offset)
        return 0
    end

    return 0, ctx
end

-- Main function.

while true do
    local event = os.pullEvents()
    local cidToResume = nil, args = {}

    if event.type == "call" then
        local itf, _ = binds[event.ctx]
        local func = itf[event.cid]

        if func == nil then
            os.answer(event.cid, 253)
        else
            calls[event.cid] = coroutine.create(func)
            cidToResume = nil
            args = event.args
        end
    elseif event.answer ==

        calls[event.cid] = coroutine.create(
end
