-- a simple process manager for thox.
--
-- Copyright (C) 2020-2021, 2024 Thomas Touhey <thomas@touhey.fr>
-- This file is part of the thox project, which is MIT-licensed.
--
-- This file contains the fundamental loading steps of the OS;
-- see <https://thox.touhey.pro/system/processes.html#startup-and-main-loop>
-- for more information.
--
-- class(new_func, methods) -- define a class in an easier way.
-- This produces a class object, which you can use the methods of
-- but not define, and initialize by simply calling.
--
-- This makes heavy use of metatables, obviously.

function class(members)
    local base = {}

    return setmetatable(base, {
        __index = members,
        __newindex = function (self, key, value)
            -- Ignore from this perspective.
        end,

        __call = function (self, ...)
            local localBase = {}
            local obj = setmetatable(
                localBase,
                setmetatable(
                    {
                        __newindex = function (self, key, value)
                            -- Allow setting localBase.
                            localBase[key] = value
                        end
                    },
                    base
                )
            )

            local initFunc = members.init
            if initFunc ~= nil then
                initFunc(obj, ...)
            end

            return obj
        end
    })
end

-- Main process manager definition.

local ProcessManager

do
    local QUANTUM = 5e6
    local SYSCALL = {
        -- The basic yield that processes can use
        -- for themselves.
        BASIC = 0,

        -- Pre-empt (timeout for a single execution),
        -- equivalent to BASIC in most cases although
        -- the process manager should not receive this.
        PREEMPT = 1,

        -- A call to spawn a process using a function has
        -- been emitted.
        SPAWN = 2,

        -- A call to exit self has been emitted.
        EXIT = 3,

        -- Process wants to pull events.
        PULL = 4,

        -- A context creation has been requested.
        CONTEXT = 5,

        -- A context closing has been requested.
        CLOSE = 6,

        -- A call to an RPC procedure has been emitted.
        CALL = 7,

        -- An answer to an RPC procedure call has been emitted.
        ANSWER = 8,

        -- A transmit request has been emitted.
        TRANSMIT = 9,
    }

    -- 16-bit number generator.
    -- This class allocates number between 1 and 2 ^ 16 - 1, for
    -- allocations. It is used:
    --
    --  * For PID (process identifier) allocations.
    --  * For CTX (context identifier) allocations.
    --  * For CID (call identifier) allocations.
    --
    -- It goes around the block.
    -- It can be optimized as well, probably.
    local NumberAllocator = class({
        init = function (self)
            self._c = 1
            self._n = 0
            self._a = {}
        end,

        alloc = function (self)
            local n = nil

            if self._n == 65535 then
                return n
            end

            while true do
                n = self._c
                self._c = self._c + 1
                if self._c > 65535 then
                    self._c = 1
                end

                if self._a[n] == nil then
                    break
                end
            end

            self._a[n] = true
            self._n = self._n + 1

            return n
        end,

        free = function (self, n)
            self._n = self._n - 1
            self._a[n] = nil
        end
    })

    local Process = class({
        RUNNING = 1, -- Running, to be resumed.
        WAITING = 2, -- Waiting for calls, alarms and events.
        ZOMBIE = 3,  -- Waiting to be killed.

        init = function (self)
            -- Members used in this scope:
            --
            --  * `answer`: the next answer to be transmitted to the
            --    process (to the previous system call).
            --  * `filters`: event filter when waiting.
            --  * `events`: events to be read by the process.
            --
            -- TODO: events need a maximum size for security, so we have
            -- to find out what we should do amongst:
            --
            --  * Deleting old events (or only old pub/sub events, RPC
            --    events are important!).
            --  * Obliging a process to read events before being able
            --    to do anything else.
            --  * Storing events in process memory, and have OOM clean the
            --    process up in case of any problems.
            --  * Not setting any limit of any kind.
            --  * Not putting pub/sub events here, instead keeping indexes
            --    on common queues (would save memory on all pub/sub
            --    queues, but would slow down access time for these events
            --    as we have to go check for all queues everytime).
            --  * Another solution?

            self.answer = {}
            self.filters = {}
            self.events = {}
            self.calls = {}

            return {}
        end
    })

    ProcessManager = class({
        init = function (self)
            -- Variables used in this scope:
            --
            --  * `pidalloc`: PID allocator, used for creating new processes.
            --  * `p`: process table, associating pids to process objects.
            --  * `ctx`: contexts managed by the kernel.

            self.pidalloc = NumberAllocator()
            self.p = {}
        end,

        -- log(...): log a message about what the process manager is doing.
        -- For debug purposes.

        log = function (self, ...)
            print("[pm " .. string.format("%.02f", os.clock()) .. "]", ...)
        end,

        -- spawn(program[, options]): add a program to be run as process.
        --
        -- The "program" argument is the string containing the program's
        -- code in plain text. It is considered untrusted by default,
        -- and is isolated as much as the permission level allows it.
        --
        -- The "options" argument, if present, should be a table containing
        -- the spawned process data. Recognized keys are the following:
        --
        --  * "commandLine": the command line as a sequence of strings,
        --    where the first argument should always be present and represents
        --    the name with which the process is spawned. By default, the
        --    command line is {"(unknown)"}, although a process can set its
        --    own command line later for debug printing purposes.
        --
        -- System calls are described in the description of the run() method.
        -- Notice that user functions are first sandboxed.
        --
        -- FIXME: check for load() syntax errors.
        -- FIXME: check for the arguments' types & constraints, arg validation.
        spawn = function (self, func, options)
            local pid, env
            local globals

            -- First of all, get the next identifier available.

            pid = self.pida.alloc()
            if pid == nil then
                error("no available pid")
            end

            -- prevent_from_hanging(co): prevent a coroutine from hanging
            -- by installing a hook on it.
            --
            -- Installs a hook using `debug.sethook` that yields
            -- periodically.
            local function prevent_from_hanging(co)
                if debug ~= nil and debug.sethook ~= nil then
                    local function hook_autoyield(type, arg)
                        coroutine.yield(SYSCALL.PREEMPT)
                    end

                    debug.sethook(co, hook_autoyield, "", QUANTUM)
                end

                return co
            end

            -- Here, we define the environment with its globals.
            -- The environment here is a table with a fallback on a "globals"
            -- table that is unique to the program for security reasons,
            -- because the process could modify the globals and bypass
            -- its sandbox (especially user processes).
            globals = {}
            env = {}

            setmetatable(env, {__index = globals})
            env._ENV = env
            env._G = globals

            -- Here, we create the basic environment. This chunk actually
            -- depends on the process type:
            --
            --  * kernel processes start with all of the available APIs,
            --    including the native Java APIs.
            --  * user processes only start with functions defined as
            --    safe for sandboxing, as they do not influence other;
            --    processes nor the process manager we're currently
            --    defining.
            --
            -- References:
            --
            --  * http://lua-users.org/wiki/SandBoxes
            --  * https://www.lua.org/manual/5.3/manual.html#6
            --  * https://github.com/SquidDev-CC/CC-Tweaked/blob/mc-1.15.x
            --    /src/main/resources/data/computercraft/lua/bios.lua
            --
            -- TODO: review what should be provided here.
            do
                local function rcopy(dest, src)
                    for key, value in pairs(src) do
                        if key == "_G" or key == "_ENV" then
                            -- We avoid copying it.
                        elseif type(value) == "table" then
                            dest[key] = {}
                            rcopy(dest[key], value)
                        else
                            dest[key] = src[key]
                        end
                    end
                end

                for _, key in pairs({"assert", "error", "ipairs",
                    "next", "pairs", "pcall", "tonumber", "tostring",
                    "type", "_VERSION", "xpcall"}) do
                    globals[key] = _G[key]
                end

                for _, module in pairs({"os", "string", "table",
                    "math", "utf8", "coroutine"}) do
                    globals[key] = {}
                end

                for _, key in pairs({"clock", "difftime", "time"}) do
                    globals.os[key] = _G.os[key]
                end

                for _, key in pairs({"byte", "char", "find", "format",
                    "gmatch", "gsub", "len", "lower", "match",
                    "rep", "reverse", "sub", "upper"}) do
                    globals.string[key] = _G.string[key]
                end

                for _, key in pairs({"insert", "maxn", "remove",
                    "sort"}) do
                    globals.table[key] = _G.table[key]
                end

                for _, key in pairs({"abs", "acos", "asin", "atan",
                    "atan2", "ceil", "cos", "cosh", "deg", "exp",
                    "floor", "fmod", "frexp", "huge", "ldexp", "log",
                    "log10", "max", "min", "modf", "pi", "pow", "rad"}) do
                    globals.math[key] = _G.math[key]
                end

                for _, key in pairs({"char", "charpattern", "codes",
                    "codepoint", "len", "offset"}) do
                    globals.utf8[key] = _G.utf8[key]
                end

                for name, func in pairs(_G.coroutine) do
                    globals.coroutine[name] = func
                end

                -- TODO: we do not always want to copy the bios
                -- functions to the process, permissions would be
                -- welcome I think.
                globals.bios = {}
                rcopy(globals.bios, _G.bios)
            end

            -- We actually need to hijack the normal work of the
            -- coroutine module to inject a few things.
            -- Indeed, when coroutine yields, with thox, it can be
            -- one of two things:
            --
            --  * A system call, as thox makes use of coroutines for
            --    its threads.
            --  * A normal yield, for internal coroutines uses.
            --
            -- For hijacking, we actually yield a first argument that
            -- is hidden to the user, which is 1 in the first case
            -- and 0 in the second; unless the function yields when
            -- returning, in which case there is no special argument
            -- because it can not be a system call.
            --
            -- References:
            --
            --  * http://www.lua.org/manual/5.3/manual.html#2.6
            --  * http://www.lua.org/manual/5.3/manual.html#6.2
            do
                local cocreate = coroutine.create
                local coresume = coroutine.resume
                local coyield = coroutine.yield
                local costatus = coroutine.status

                globals.coroutine = {}

                globals.coroutine.create = function (func)
                    return prevent_from_hanging(cocreate(func))
                end

                globals.coroutine.yield = function (...)
                    local args = table.pack(...)
                    table.insert(args, 1, SYSCALL.BASIC)

                    return coyield(table.unpack(args))
                end

                globals.coroutine.resume = function (co, ...)
                    local args = table.pack(...)

                    while true do
                        local retpack = table.pack(
                            coresume(co, table.unpack(args))
                        )

                        if costatus(co) == "dead" then
                            -- The function returned its final yield,
                            -- which means we just return what it has
                            -- returned.
                            return table.unpack(retpack)
                        elseif retpack[1] == true then
                            -- The function has just yielded, which
                            -- means the special argument is there,
                            -- let's treat it!
                            local ret = table.remove(retpack, 1)
                            local typ = table.remove(retpack, 1)

                            if typ == SYSCALL.BASIC then
                                -- A normal value.
                                table.insert(retpack, 1, ret)
                                return table.unpack(retpack)
                            else
                                -- A system yield, we just yield it for
                                -- the parent coroutine to catch it to
                                -- transmit it to the main loop, then
                                -- come back here.
                                table.insert(retpack, 1, typ)
                                args = table.pack(
                                    coyield(table.unpack(retpack))
                                )
                            end
                        else
                            -- An error has occurred, so we just
                            -- return it as is.
                            return table.unpack(retpack)
                        end
                    end
                end

                -- System calls.
                local toCanonicalName, toValidName, toValidArgs

                do
                    local vc = "[a-z][a-z0-9_]*"
                    local vn = ("^" .. vc .. "(.(" .. vc .. ".)*"
                        .. vc .. ")?$")
                    local reservedNames = {"", "and", "break", "do",
                        "else", "elseif", "end", "false", "for",
                        "function", "goto", "if", "in", "local", "nil",
                        "not", "or", "repeat", "return", "then", "true",
                        "until", "while"}

                    toCanonicalName = function (name)
                        name = string.lower(tostring(name))
                        if name == "" then
                            return nil
                        end

                        return name
                    end

                    toValidName = function (name)
                        name = string.lower(tostring(name))

                        if string.match(name, vn) ~= name then
                            return nil
                        end

                        for _, reserved in pairs(reservedNames) do
                            if string.find("." .. name .. ".",
                                "." .. reserved .. ".") ~= nil then
                                return nil
                            end
                        end

                        return name
                    end

                    toValidArgs = function (args)
                        local sanitizedArgs = {}

                        -- What we're trying to do here is to eliminate
                        -- complicated types to only keep simple types
                        -- as arguments, for now.
                        --
                        -- For now, we do not allow tables because they
                        -- can have custom metatables. This is not what
                        -- we want for now.

                        for i, value in ipairs(args) do
                            local typ = type(value)

                            if typ == "number"
                            or typ == "string"
                            or typ == "boolean" then
                                table.insert(sanitizedArgs, value)
                            else
                                return nil
                            end
                        end

                        return sanitizedArgs
                    end
                end

                -- Process-related calls.

                local run_func(func, options)
                    -- TODO: validate that ``func`` is a function,
                    -- has no upvalues (or upvalues are replaced
                    -- with nil), and that options is a table and
                    -- copy the values instead of transmitting them
                    -- by reference.

                    return coyield(SYSCALL.SPAWN, func, {})
                end

                globals.os.run = run_func

                globals.os.run_file = function (path, options)
                    local path = ("\"" ..
                        string.gsub(string.gsub(path, "\\", "\\\\"),
                            "\"", "\\\"") .. "\"")
                    local code = (
                        "local w = load(io.open(" .. path .. ")"
                        .. ":read(\"a\"), " .. path .. ", \"t\")"
                        .. "\r\nif w == nil then os.exit(false, true) end"
                        .. "\r\nreturn w()"
                    )

                    return run_func(load(code, "t"))
                end

                globals.os.run_code = function (code, options)
                    return run_func(load(code, "t"))
                end

                -- Context-related calls.

                globals.os.context = function ()
                    return coyield(SYSCALL.CONTEXT)
                end

                globals.os.close = function (ctx)
                    return coyield(SYSCALL.CLOSE, ctx)
                end

                globals.os.pull = function (...)
                    -- TODO: validate the filters.
                    return coyield(SYSCALL.PULL)
                end

                globals.os.call = function (ctx, name, ...)
                    local name = toCanonicalName(name)
                    local args = toValidArgs(table.unpack(...))

                    if name == nil or args == nil then
                        return false
                    end

                    -- TODO: check if the given context exists.
                    return coyield(SYSCALL.CALL, name, table.unpack(args))
                end

                globals.os.answer = function (cid, ...)
                    local cid = math.tointeger(cid)
                    local args = toValidArgs(table.unpack(...))

                    if name == nil or args == nil then
                        return false
                    end

                    return coyield(SYSCALL.ANSWER, name, table.unpack(args))
                end

                globals.os.capture = function (co, ...)
                    local args = table.pack(...)

                    while true do
                        local retpack = table.pack(
                            coresume(co, table.unpack(args))
                        )

                        if costatus(co) == "dead" then
                            -- The function returned its final yield,
                            -- which means we just return what it has
                            -- returned.
                            return "yield", table.unpack(retpack)
                        elseif retpack[1] == true then
                            -- The function has just yielded, which
                            -- means the special argument is there,
                            -- let's treat it!
                            local ret = table.remove(retpack, 1)
                            local typ = table.remove(retpack, 1)

                            if typ == SYSCALL.BASIC then
                                -- A normal value.
                                table.insert(retpack, 1, ret)
                                return "yield", table.unpack(retpack)
                            elseif typ == SYSCALL.PREEMPT then
                                -- Pre-empt, let's just yield for this one.
                                coyield(SYSCALL.PREEMPT)
                            elseif typ == SYSCALL.SPAWN then
                                local func, opt = retpack
                                return "run", func, opt
                            elseif typ == SYSCALL.EXIT then
                                return "exit", retpack[1], retpack[2]
                            elseif typ == SYSCALL.PULL then
                            elseif typ == SYSCALL.CALL then
                                -- A call, we want to return it!
                                return "call", table.unpack(retpack)
                            elseif typ == SYSCALL.ANSWER then
                                -- An answer, we want to return it as well.
                                return "answer", table.unpack(retpack)
                            elseif typ == SYSCALL.BIND then
                                -- A bind, we want to return it as well.
                                return "bind", retpack[1]
                            else
                                -- TODO: unsupported system call?
                            end
                        else
                            -- An error has occurred, so we just
                            -- return it as is.
                            return table.unpack(retpack)
                        end
                    end
                end

                -- TODO: check for debug functions, maybe?
            end

            -- The program has been loaded with the correct environment,
            -- we can now create the process with the given function.
            local p = Process()

            p.thread = prevent_from_hanging(coroutine.create(func))
            p.state = Process.RUNNING

            self.p[pid] = p
            return pid
        end,

        -- kill(pid): kill a process.
        -- In this function, we only set the processes as "zombie", we
        -- do not remove the processes from the process list as they
        -- are removed in run().
        --
        -- As we do this, we need to check for processes that watch this
        -- one in order to check if we make them die too.
        kill = function (self, pid)
            local p = self.processes[pid]

            if p.state == Process.ZOMBIE then
                return
            end

            p.state = Process.ZOMBIE
        end,

        -- run(): run the main loop of processes.
        -- For more information on what this function does, see
        -- <https://thox.touhey.pro/system/processes.html#pm.ProcessManager.run>.
        --
        -- FIXME: if all processes are sleeping, let's sleep until we have
        --        reached the minimum duration.
        run = function(self)
            -- Main process management loop.
            while true do
                local zombies = {}
                local sleepingTime = nil

                for pid, process in pairs(self.p) do
                    -- First of all, check for any alarm.
                    while #process.alarms do
                        local alm = next(process.alarms)

                        if alm.alarmAt < os.clock() then
                            break
                        end

                        -- Let's remove the alarm then.
                        table.remove(process.alarms, 1)
                        process.calls.remove(alm.cid)
                        table.insert(process.events, {
                            type = "answer",
                            cid = alm.cid
                        })
                    end

                    -- Then, let's check if the process is currently waiting.
                    if process.state == Process.WAITING then
                        if #process.events then
                            process.answer = {table.remove(process.events, 1)}
                            process.state = Process.RUNNING
                        end
                    end

                    -- Then resume the process if should be.
                    if process.state == Process.RUNNING then
                        local ret, sta

                        do
                            local answer = process.answer

                            process.answer = {}
                            ret = table.pack(coroutine.resume(process.thread,
                                table.unpack(answer)))
                            sta = coroutine.status(process.thread)
                        end

                        local answer = {}

                        -- First, let's check the state of the coroutine,
                        -- to check if the coroutine is ready to get up
                        -- and running again on next round.
                        -- Then, we check if the process has sent us a
                        -- signal that it wants to either send a message,
                        -- pull or poll a message, or sleep.
                        if ret[1] and sta == "dead" then
                            -- The coroutine has died of old age.
                            -- That's fine, it will allow future generations
                            -- to prosper on the same grounds.

                            process.state = "zombie"
                            table.insert(zombies, pid)

                            self:log("[" .. pid .. "] died from natural cause")
                        elseif not ret[1] then
                            -- An error has occurred in the coroutine,
                            -- which has stopped consequently.
                            -- Someone is going to be fired!
                            --
                            -- FIXME: put that on some debugging thread,
                            -- maybe?

                            self:log("[" .. pid .. "] died from error:")
                            self:log("  " .. ret[2])

                            process.state = "zombie"
                            table.insert(zombies, pid)
                        elseif ret[2] == SYSCALL.BASIC
                        or ret[2] == SYSCALL.PREEMPT then
                            -- Either the user has called coroutine.yield()
                            -- in the main function, or it has been called
                            -- by the debug hook function after a count for
                            -- pre-emptive.
                            --
                            -- We're going to take this as an opportunity
                            -- to run other processes.

                            self:log("[" .. pid .. "] has been pre-empted")
                        elseif ret[2] == SYSCALL.CALL then
                            local name = ret[3]
                            local args = ret

                            -- We want to pop everything that's not the args,
                            -- and that includes:
                            --
                            --  * the coroutine.resume() boolean status.
                            --  * the custom coroutine.yield first value (0/1).
                            --  * the verb.

                            table.remove(args, 1)
                            table.remove(args, 1)
                            table.remove(args, 1)

                            -- Now we can execute whatever the user wanted.

                            if name == "alarm" then
                                -- The process wants to be woken up in
                                -- some time, ok, let's do that then.

                                local duration = args[1]

                                if type(duration) ~= "number"
                                or duration ~= duration or duration < 0 then
                                    answer = {false, {
                                        message = "first argument should be "
                                            .. "a positive number (duration)"}}
                                else
                                    -- Let's just add an alarm.

                                    local aat = os.clock() + duration
                                    local cid = process.calls.add(
                                        Call(Call.ALARM))

                                    if cid == nil then
                                        answer = {false, {
                                            message = "no CID available"}}
                                    else
                                        local ai, a, ap = nil

                                        for ai, a in ipairs(process.alarms) do
                                            if a.alarmAt < aat then
                                                ap = ai
                                                break
                                            end
                                        end

                                        table.insert(process.alarms, ap,
                                            Alarm(cid, aat))
                                    end
                                end
                            elseif verb == "print" then
                                -- Print a message! TODO: debug, remove this

                                print(table.unpack(args))
                                answer = {true, {}}
                            else
                                -- TODO: check the binds from here, emit a
                                -- call, etc.

                                answer = {false, {
                                    message = "unbound rpc call"}}
                            end
                        else
                            -- TODO: answer, else.

                            answer = {false, {
                                message = "doesn't work yet"}}
                        end

                        process.answer = answer
                    end
                end

                -- Check the "hardware" events, using a timer. We want to
                -- get all events within the timer timespan, which means
                -- we'll get worth the duration of events at once.
                --
                -- FIXME: is the timer regular? not sure if I'm supposed to
                -- cancel it after it fires once or not…
                -- TODO: other events…
                --
                -- References:
                --
                --  * https://tweaked.cc/module/os.html#v:startTimer
                --  * https://tweaked.cc/module/os.html#v:cancelTimer
                --  * http://computercraft.info/wiki/Os.pullEvent#Event_types

                if sleepingTime == nil then
                    sleepingTime = .05
                end

                do
                    local timer = bios.os.startTimer(sleepingTime)

                    while true do
                        local event, param1, param2, param3 = bios.os.pullEventRaw()

                        if event == "timer" then
                            bios.os.cancelTimer(timer)
                            break
                        end
                    end
                end

                -- Remove the zombies!

                for _, pid in pairs(zombies) do
                    self.p[pid] = nil
                end

                -- Check if we should exit here.

                do
                    local hasp = false

                    for key, _ in pairs(self.p) do
                        hasp = true
                        break
                    end

                    if not hasp then
                        break
                    end
                end
            end
        end,
    })
end

-- Run the process manager with the initial tasks.

do
    local pm = ProcessManager()

    local function exec(path, name)
        local h = bios.fs.open(path, "r")
        local prog = h:readAll()

        h.close()
        pm:spawn(prog, {
            isKernel = 0,
            commandLine = {name}})
    end

    exec("/bin/init.lua", "/bin/init.lua")
    exec("/bin/hello.lua", "/bin/hello.lua")

    --exec("/bin/hello_coroutine.lua", "/bin/hello_coroutine")
    --exec("/bin/the_for.lua", "/bin/the_for")

    pm:run()
end

-- Should work nicely now.
