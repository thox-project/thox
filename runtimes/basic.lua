-- Add a little magic `rpc` module, that does synchronous
-- calls to the system for the basic programs to work easily.

do
    local function namespaceComponent(key)
        key = string.lower(tostring(key))

        for _, reserved in pairs({"", "and", "break", "do", "else",
            "elseif", "end", "false", "for", "function",
            "goto", "if", "in", "local", "nil", "not",
            "or", "repeat", "return", "then", "true",
            "until", "while"}) do
            if key == reserved then
                return nil
            end
        end

        if string.match(key, "^[a-z][a-z0-9_]*$") ~= key then
            return nil
        end

        return key
    end

    -- Now for the main RPC proxy.

    local function rpcNamespace(ns)
        return setmetatable({
            _ns = ns
        }, {
            __index = function (self, key)
                -- Here, we want a namespace proxy for calling
                -- the systems proxy.

                local ns = self.__ns
                local key = namespaceComponent(key)

                if key == nil then
                    return nil
                end

                if ns == "" then
                    ns = key
                else
                    ns = ns .. "." .. key
                end

                return rpcNamespace(ns)
            end,

            __newindex = function(self, key, value)
                error("please use the daemon module for "
                    .. "binding an RPC call")
            end,

            __call = function (self, ...)
                -- Here, we want to make the call and wait
                -- for its answer.

                if ns == "" then
                    error("cannot call the rpc module")
                else
                    local cid = globals.os.call(ns,
                        table.unpack(...))

                    while true do
                        local event = os.pull()

                        if event.type == "answer"
                        and event.id == cid then
                            return table.unpack(event.args)
                        end
                    end
                end
            end
        })
    end

    _G.rpc = rpcNamespace("")
end


-- Add a few system calls in, which would normally be unsafe
-- but we make them pass through the current function.

_G.print = function (...)
    local args = table.pack(...)

    for key, value in args do
        args[key] = tostring(value)
    end

    _G.rpc.print(table.unpack(args))
end

_G.sleep = function (duration)
    local duration = tonumber(duration)

    _G.rpc.alarm(duration)
end
